---
codename: "s3ve3g"
name: "Samsung S3 Neo"
comment: "community device"
icon: "phone"
image: "https://upload.wikimedia.org/wikipedia/commons/d/d6/Samsung_Galaxy_S_III.png"
maturity: .8
---

## Legacy ports: Samsung S3 Neo

- based on Halium 5.1
- Reference device exploring the Samsung device space
- Currently no working camera, hotspot (but could work later on)
- Device does reboot sometimes after powering on, but will eventually boot into the OS

Discussion forum: https://forums.ubports.com/category/62/samsung-s3-neo
