---
codename: "enchilada, fajita"
name: "OnePlus 6/6T"
comment: "community device"
icon: "phone"
image: "https://wiki.lineageos.org/images/devices/enchilada.png"
noinstall: true
maturity: .7
---

## Device specifications

| Component | Details                                            |
| --------: | -------------------------------------------------- |
|       SoC | Qualcomm SDM845 Snapdragon 845                     |
|       RAM | 6/8 GB LPDDR4X                                     |
|       CPU | Octa-core (4x 2.8 GHz Kryo 385 Gold & 4x 1.7 GHz Kryo 385 Silver) |
|       GPU | Adreno 630                                         |
|   Storage | 128/256 GB UFS2.1                                  |
|    Camera | 16 MP                                              |
|   Battery | Non-removable Li-Po 3400 / 3700 mAh                |

## Port status

The port is being actively developed, with hopes to bring it to the installer.

### Issues

* Poor idle drain due to `CONFIG_PM_AUTOSLEEP` being disabled to prevent crashes.
* Camera doesn't seem to function?
* SIM slot 1 doesn't work, workaround by moving your SIM card to slot 2.

## Maintainers

calebccff, MrCyjanek

## Source repos

* [Kernel sources](https://gitlab.com/ubports/community-ports/android9/oneplus-6/kernel-oneplus-sdm845)
* [Device overlay](https://gitlab.com/ubports/community-ports/android9/oneplus-6/oneplus-enchilada-fajita)