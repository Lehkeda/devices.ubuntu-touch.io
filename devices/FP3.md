---
codename: "FP3"
name: "Fairphone 3"
comment: "community device"
icon: "phone"
image: "https://live.staticflickr.com/65535/50801596512_9b846d1a60_k.jpg"
noinstall: true
maturity: .9
---

### Device specifications

|               Component | Details                                                               |
| ----------------------: | --------------------------------------------------------------------- |
|                 Chipset | Qualcomm MSM8953 Snapdragon 632                                       |
|                     CPU | Octa-Core Kryo 250 1.8GHz                                             |
|            Architecture | arm64                                                                 |
|                     GPU | Adreno 506 650MHz                                                     |
| Shipped Android Version | 9.0 (Pie)                                                             |
|                 Screen  | 143 mm (5.65 in)<br>2160x1080 (427 PPI)<br>LCD IPS Touchscreen        |
|                 Storage | 64 GB                                                                 |
|                  Memory | 4 GB                                                                  |
|                 Battery | 3040 mAh                                                              |
|                 Cameras | 12 MP, LED flash<br>8 MP, No flash                                    |
|              Dimensions | 158 mm (6.22 in) (h)<br>71.8 mm (2.83 in) (w)<br>9.89 mm (0.39 in) (d)|
|                  Weight | 187,4 g                                                               |

### Maintainer(s)

luksus42

### Forum topic

https://forums.ubports.com/topic/5373/fairphone-3-fp3-port

### Source repos

https://gitlab.com/ubports/community-ports/android9/fairphone-3/fairphone-fp3

### CI builds

https://gitlab.com/ubports/community-ports/android9/fairphone-3/fairphone-fp3/-/pipelines

### Notes

Vibration actor does not work yet.\
Anbox is currently not supported on arm64.\
Automatic brightness is not working.